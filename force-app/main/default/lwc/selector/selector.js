import { LightningElement, track, api } from 'lwc';

export default class Selector extends LightningElement {
    @track selectedProductId;

    handleProductSelected(evt) {
        console.log('4th step - selector component listen for list events and call handleProductSelected');
        console.log('5th step - selector.js get the ID and set it to the selectedProductId in c-detail');
        console.log('aha')
        this.selectedProductId = evt.detail;
    }
}
