import { LightningElement, track } from 'lwc';
import { bikes } from 'c/data';

export default class List extends LightningElement {
    bikes = bikes;

    handleTileClick(evt) {
        console.log(evt.detail);
        console.log('2nd step - List component listen for c-tile calls and handle the sended ID.')
        console.log('3th step - List component creates another event with that ID and dispatches the event to JS');
        // This component wants to emit a productselected event to its parent
        const event = new CustomEvent('productselected', {
            detail: evt.detail
        });
        // Fire the event from c-list
        this.dispatchEvent(event);
    }
}
