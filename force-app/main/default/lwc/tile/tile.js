import { LightningElement, api, track } from 'lwc';

export default class Tile extends LightningElement {
    @api product;

    tileClick() {
        console.log(this.product.fields.Id.value);
        console.log('1st step - tile.js Fire the  event to send seleted ID from and to List component')
        const event = new CustomEvent('tileclick', {
            // detail contains only primitives
            detail: this.product.fields.Id.value
        });
        // Fire the event from c-tile
        this.dispatchEvent(event);
    }
}
